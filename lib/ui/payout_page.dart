import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:inside_track/bloc/payout/payout_bloc.dart';
import 'package:inside_track/bloc/payout/payout_event.dart';
import 'package:inside_track/bloc/payout/payout_state.dart';
import 'package:inside_track/model/horse.dart';
import 'package:numberpicker/numberpicker.dart';

class PayoutPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocProvider<PayoutBloc>(
      builder: (_) => PayoutBloc(),
      child: Scaffold(
        appBar: AppBar(
          title: Text("Inside Track Payout"),
        ),
        body: SafeArea(
          child: Container(
            child: BlocBuilder<PayoutBloc, PayoutState>(builder: (context, state) {
              return Column(
                children: List<Widget>.generate(
                  state.horses.length,
                  (index) => buildHorseSelector(context, index, state.horses[index]),
                )
                  ..add(Container(height: 4))
                  ..add(buildPayout(context, state.payout)),
              );
            }),
          ),
        ),
      ),
    );
  }

  Widget buildHorseSelector(BuildContext context, int index, Horse horse) {
    return Expanded(
      flex: 1,
      child: Card(
        elevation: 5,
        child: Row(
          children: <Widget>[
            Expanded(
              flex: 4,
              child: Center(
                child: Text(
                  "Horse #${index + 1} Odds",
                  style: TextStyle(
                    fontSize: 20,
                  ),
                ),
              ),
            ),
            Expanded(
              flex: 7,
              child: Center(
                child: NumberPicker.horizontal(
                  initialValue: horse.odd,
                  minValue: horse.minOdd,
                  maxValue: horse.maxOdd,
                  onChanged: (value) => BlocProvider.of<PayoutBloc>(context).add(PayoutEvent.updateOdd(index, value)),
                  itemExtent: 70,
                  listViewHeight: 50,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget buildPayout(BuildContext context, int payout) {
    return Expanded(
      flex: 1,
      child: Container(
        decoration: BoxDecoration(
            borderRadius: new BorderRadius.vertical(
              top: const Radius.circular(10.0),
            ),
            color: Theme.of(context).primaryColor),
        child: Center(
            child: Text(
          "Payout : $payout%",
          style: TextStyle(
            fontSize: 40,
          ),
        )),
      ),
    );
  }
}
