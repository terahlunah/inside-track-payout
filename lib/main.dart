import 'package:flutter/material.dart';
import 'package:inside_track/ui/payout_page.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Inside Track Payout',
      home: PayoutPage(),
      theme: new ThemeData(
        primarySwatch: Colors.deepPurple,
        brightness: Brightness.dark
      ),
    );
  }
}
