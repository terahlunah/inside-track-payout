


extension ObjectExt on Object {
  T let<T, R>(T block(T)) => block(this);
}