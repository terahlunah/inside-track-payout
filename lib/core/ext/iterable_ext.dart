


extension IterableExt<T> on Iterable<T> {

  T get firstOrNull => this.isNotEmpty ? this.first : null;

}

extension IterableNumExt on Iterable<num> {
  num sum() => this.fold(0, (a, b) => a + b);
}