import 'package:inside_track/model/horse.dart';

class PayoutState {
  List<Horse> horses;
  int payout;

  PayoutState(this.horses, this.payout);
}
