abstract class PayoutEvent {
  PayoutEvent();

  factory PayoutEvent.updateOdd(int horse, int odd) => HorseOddUpdate(horse, odd);
}

class HorseOddUpdate extends PayoutEvent {
  int horse;
  int odd;

  HorseOddUpdate(this.horse, this.odd);
}
