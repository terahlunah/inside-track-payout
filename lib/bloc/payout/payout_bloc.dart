import 'package:bloc/bloc.dart';
import 'package:inside_track/bloc/payout/payout_event.dart';
import 'package:inside_track/bloc/payout/payout_state.dart';
import 'package:inside_track/core/ext/iterable_ext.dart';
import 'package:inside_track/model/horse.dart';

class PayoutBloc extends Bloc<PayoutEvent, PayoutState> {
  final _initialHorses = [
    Horse(1, 1, 5),
    Horse(3, 1, 5),
    Horse(9, 6, 15),
    Horse(14, 6, 15),
    Horse(20, 16, 30),
    Horse(25, 16, 30),
  ];

  @override
  PayoutState get initialState => PayoutState(_initialHorses, _computePayout(_initialHorses));

  @override
  Stream<PayoutState> mapEventToState(PayoutEvent event) async* {
    if (event is HorseOddUpdate) {
      var horses = state.horses;
      horses[event.horse].odd = event.odd;
      yield PayoutState(horses, _computePayout(horses));
    }
  }

  int _computePayout(List<Horse> odds) {
    final total = odds.map((it) => 1.0 / (1.0 + it.odd)).sum();
    final payout = (100.0 / (total) - 100.0).round();
    return payout;
  }
}
